package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/common_utils/middleware"
	"gitlab.com/bishe-projects/demo_commodity_api/handler"
)

func main() {
	r := gin.Default()
	r.Use(middleware.Cors())
	commodity := r.Group("/commodity")
	{
		commodity.POST("/getById", handler.GetCommodityById)
		commodity.POST("/getList", handler.GetCommodityList)
		commodity.POST("/getRankList", handler.GetCommodityRankList)
		commodity.POST("/getCommentList", handler.GetCommodityCommentList)
		commodity.POST("/hasFavorite", middleware.VerifyTokenMiddleware(), handler.HasFavoriteCommodity)
		commodity.POST("/favorite", middleware.VerifyTokenMiddleware(), handler.FavoriteCommodity)
		commodity.POST("/favoriteCancel", middleware.VerifyTokenMiddleware(), handler.FavoriteCancelCommodity)
		commodity.POST("/getFavoriteList", middleware.VerifyTokenMiddleware(), handler.GetFavoriteList)
		commodity.POST("/buy", middleware.VerifyTokenMiddleware(), handler.BuyCommodity)
	}
	order := r.Group("/order", middleware.VerifyTokenMiddleware())
	{
		order.POST("/getList", handler.GetOrderList)
		order.POST("/comment", handler.CommentOrder)
	}
	r.Run("0.0.0.0:8087")
}

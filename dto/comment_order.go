package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type CommentOrder struct {
	OrderId int64  `json:"order_id" binding:"required"`
	Content string `json:"content" binding:"required"`
}

func (d *CommentOrder) ConvertToReq() *demo_commodity.CommentOrderReq {
	return &demo_commodity.CommentOrderReq{
		OrderId: d.OrderId,
		Content: d.Content,
	}
}

package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type FavoriteCommodity struct {
	CommodityId int64 `json:"commodity_id" binding:"required"`
}

func (d *FavoriteCommodity) ConvertToReq() *demo_commodity.FavoriteCommodityReq {
	return &demo_commodity.FavoriteCommodityReq{
		CommodityId: d.CommodityId,
	}
}

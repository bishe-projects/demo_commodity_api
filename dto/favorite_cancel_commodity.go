package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type FavoriteCancelCommodity struct {
	CommodityId int64 `json:"commodity_id" binding:"required"`
}

func (d *FavoriteCancelCommodity) ConvertToReq() *demo_commodity.FavoriteCancelCommodityReq {
	return &demo_commodity.FavoriteCancelCommodityReq{
		CommodityId: d.CommodityId,
	}
}

package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type BuyCommodity struct {
	CommodityId int64 `json:"commodity_id" binding:"required"`
	Amount      int64 `json:"amount" binding:"required"`
}

func (d *BuyCommodity) ConvertToReq() *demo_commodity.BuyCommodityReq {
	return &demo_commodity.BuyCommodityReq{
		CommodityId: d.CommodityId,
		Amount:      d.Amount,
	}
}

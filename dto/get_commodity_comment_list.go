package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type GetCommodityCommentList struct {
	CommodityId int64 `json:"commodity_id" binding:"required"`
}

func (d *GetCommodityCommentList) ConvertToReq() *demo_commodity.GetCommodityCommentListReq {
	return &demo_commodity.GetCommodityCommentListReq{
		CommodityId: d.CommodityId,
	}
}

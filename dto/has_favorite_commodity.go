package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type HasFavoriteCommodity struct {
	CommodityId int64 `json:"commodity_id" binding:"required"`
}

func (d *HasFavoriteCommodity) ConvertToReq() *demo_commodity.HasFavoriteCommodityReq {
	return &demo_commodity.HasFavoriteCommodityReq{
		CommodityId: d.CommodityId,
	}
}

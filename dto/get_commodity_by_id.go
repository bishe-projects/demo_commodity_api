package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type GetCommodityById struct {
	CommodityId int64 `json:"commodity_id" binding:"required"`
}

func (d *GetCommodityById) ConvertToReq() *demo_commodity.GetCommodityByIdReq {
	return &demo_commodity.GetCommodityByIdReq{
		CommodityId: d.CommodityId,
	}
}

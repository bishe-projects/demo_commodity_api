package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity/commodityservice"
)

var (
	CommodityClient = commodityservice.MustNewClient("demo_commodity_service", client.WithHostPorts("0.0.0.0:8890"), client.WithTransportProtocol(transport.TTHeaderFramed))
)

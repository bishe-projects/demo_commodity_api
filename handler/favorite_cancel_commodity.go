package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_commodity_api/client"
	"gitlab.com/bishe-projects/demo_commodity_api/dto"
	"net/http"
)

func FavoriteCancelCommodity(c *gin.Context) {
	var favoriteCancelCommodity dto.FavoriteCancelCommodity
	if err := c.BindJSON(&favoriteCancelCommodity); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommodityClient.FavoriteCancelCommodity(ctx, favoriteCancelCommodity.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

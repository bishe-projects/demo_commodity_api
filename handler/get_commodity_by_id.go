package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_commodity_api/client"
	"gitlab.com/bishe-projects/demo_commodity_api/dto"
	"net/http"
)

func GetCommodityById(c *gin.Context) {
	var getCommodityById dto.GetCommodityById
	if err := c.BindJSON(&getCommodityById); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	resp, err := client.CommodityClient.GetCommodityById(c, getCommodityById.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

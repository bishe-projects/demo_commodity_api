package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_commodity_api/client"
	"gitlab.com/bishe-projects/demo_commodity_api/dto"
	"net/http"
)

func GetCommodityCommentList(c *gin.Context) {
	var getCommodityCommentList dto.GetCommodityCommentList
	if err := c.BindJSON(&getCommodityCommentList); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	resp, err := client.CommodityClient.GetCommodityCommentList(c, getCommodityCommentList.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_commodity_api/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
	"net/http"
)

func GetCommodityList(c *gin.Context) {
	resp, err := client.CommodityClient.GetCommodityList(c, demo_commodity.NewGetCommodityListReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

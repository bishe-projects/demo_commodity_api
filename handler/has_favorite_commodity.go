package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_commodity_api/client"
	"gitlab.com/bishe-projects/demo_commodity_api/dto"
	"net/http"
)

func HasFavoriteCommodity(c *gin.Context) {
	var hasFavoriteCommodity dto.HasFavoriteCommodity
	if err := c.BindJSON(&hasFavoriteCommodity); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommodityClient.HasFavoriteCommodity(ctx, hasFavoriteCommodity.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_commodity_api/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
	"net/http"
)

func GetOrderList(c *gin.Context) {
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommodityClient.GetOrderList(ctx, demo_commodity.NewGetOrderListReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
